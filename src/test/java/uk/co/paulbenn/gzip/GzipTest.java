package uk.co.paulbenn.gzip;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class GzipTest {

    @Test
    void magic() {
        assertNotNull(Gzip.MAGIC);
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "az09", "!\"£$%^&*()_+-={}[]:@~;'#<>?,./|\\"})
    void deflateAndInflate(String argument) {
        byte[] deflatedArgument = Gzip.deflate(argument);
        assertEquals(argument, Gzip.inflate(deflatedArgument));
    }

    @Test
    void deflateAndInflateUtf8() {
        String helloInChinese = "\u4f60\u597d";
        byte[] deflatedArgument = Gzip.deflate(helloInChinese, StandardCharsets.UTF_8);
        assertEquals(helloInChinese, Gzip.inflate(deflatedArgument, StandardCharsets.UTF_8));
    }

    @Test
    void deflateNull() {
        assertThrows(NullPointerException.class, () -> Gzip.deflate(null));
    }

    @Test
    void inflateNull() {
        assertThrows(NullPointerException.class, () -> Gzip.inflate(null));
    }

    @Test
    void inspect() {
        byte[] deflated = Gzip.deflate("a test sentence");
        GzipEntry entry = Gzip.inspect(deflated);
        assertNotNull(entry.getHeader());
        assertEquals(deflated, entry.getBytes());
        assertNotNull(entry.getTrailer());
    }
}
