package uk.co.paulbenn.gzip;

import java.util.zip.Deflater;

public enum CompressionMethod {
    DEFLATE,
    UNKNOWN;

    public static CompressionMethod of(byte compressionMethod) {
        if (compressionMethod < 0) {
            throw new IllegalArgumentException("compression method cannot be negative");
        }

        if (compressionMethod <= 7) {
            throw new IllegalArgumentException("compression methods 0-7 are reserved");
        }

        if (compressionMethod == Deflater.DEFLATED) {
            return DEFLATE;
        }

        return UNKNOWN;
    }
}
