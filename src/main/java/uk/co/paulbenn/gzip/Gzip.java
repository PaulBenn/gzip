package uk.co.paulbenn.gzip;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public final class Gzip {

    public static final byte[] MAGIC = new byte[] {
        (byte) 0x1f,
        (byte) 0x8b,
    };

    private Gzip() { }

    public static byte[] deflate(String inflated) {
        return deflate(inflated, Charset.defaultCharset());
    }

    public static byte[] deflate(String inflated, Charset charset) {
        checkNotNull(inflated);

        ByteArrayOutputStream deflated = new ByteArrayOutputStream();

        try (GZIPOutputStream gzipOutputStream = new GZIPOutputStream(deflated)) {
            gzipOutputStream.write(inflated.getBytes(charset));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        return deflated.toByteArray();
    }

    public static String inflate(byte[] deflated) {
        return inflate(deflated, Charset.defaultCharset());
    }

    public static String inflate(byte[] deflated, Charset charset) {
        checkNotNull(deflated);

        StringBuilder inflated = new StringBuilder();

        try (Reader gzipReader = gzipReader(deflated, charset)) {
            int character;
            while ((character = gzipReader.read()) != -1) {
                inflated.append((char) character);
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        return inflated.toString();
    }

    public static GzipEntry inspect(byte[] deflated) {
        return new GzipEntry(deflated);
    }

    private static void checkNotNull(String inflated) {
        if (inflated == null) {
            throw new NullPointerException("cannot deflate null string");
        }
    }

    private static void checkNotNull(byte[] deflated) {
        if (deflated == null) {
            throw new NullPointerException("cannot inflate null byte array");
        }
    }

    private static Reader gzipReader(byte[] deflated, Charset charset) throws IOException {
        InputStream inputStream = new GZIPInputStream(new ByteArrayInputStream(deflated));
        return new BufferedReader(new InputStreamReader(inputStream, charset));
    }
}
