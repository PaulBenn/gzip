package uk.co.paulbenn.gzip;

import java.util.Arrays;

public class GzipEntry {

    private final GzipHeader header;

    private final byte[] bytes;

    private final GzipTrailer trailer;

    public GzipEntry(byte[] deflated) {
        checkNotNull(deflated);
        try {
            this.header = new GzipHeader(getHeaderBytes(deflated));
            this.bytes = deflated;
            this.trailer = new GzipTrailer(getTrailerBytes(deflated));
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new IllegalArgumentException(
                String.format(
                    "deflated byte array must be of length %d or greater",
                    GzipHeader.MIN_LENGTH + GzipTrailer.LENGTH
                ),
                e
            );
        }
    }

    private static void checkNotNull(byte[] deflated) {
        if (deflated == null) {
            throw new NullPointerException("cannot inspect null byte array");
        }
    }

    private static byte[] getHeaderBytes(byte[] deflated) {
        return Arrays.copyOfRange(deflated, 0, GzipHeader.MIN_LENGTH);
    }

    private static byte[] getTrailerBytes(byte[] deflated) {
        return Arrays.copyOfRange(deflated, deflated.length - GzipTrailer.LENGTH, deflated.length);
    }

    public GzipHeader getHeader() {
        return header;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public GzipTrailer getTrailer() {
        return trailer;
    }

    @Override
    public String toString() {
        return "GzipEntry (" + bytes.length + " bytes)";
    }
}
