package uk.co.paulbenn.gzip;

import java.nio.ByteBuffer;
import java.time.Instant;
import java.util.Arrays;
import java.util.zip.Deflater;

public class GzipHeader {

    public static final int MIN_LENGTH = 10;

    private static final byte ID1 = Gzip.MAGIC[0];
    private static final byte ID2 = Gzip.MAGIC[1];

    private static final byte[] NO_TIMESTAMP_AVAILABLE = new byte[] { 0, 0, 0, 0 };

    private final byte CM;
    private final CompressionMethod compressionMethod;

    private final byte FLG;
    private final Flags flags;

    private final byte[] MTIME;
    private final Instant modificationTime;

    private final byte XFL;

    private final byte OS;
    private final OperatingSystem operatingSystem;

    public GzipHeader() {
        this.CM = Deflater.DEFLATED;
        this.compressionMethod = CompressionMethod.DEFLATE;

        this.FLG = 0;
        this.flags = Flags.NO_FLAGS;

        this.MTIME = NO_TIMESTAMP_AVAILABLE;
        this.modificationTime = null;

        this.XFL = 0;

        this.OS = 0;
        this.operatingSystem = OperatingSystem.UNKNOWN;
    }

    public GzipHeader(byte[] gzipHeader) {
        checkHeaderValidity(gzipHeader);

        this.CM = gzipHeader[2];
        this.compressionMethod = CompressionMethod.of(this.CM);

        this.FLG = gzipHeader[3];
        this.flags = Flags.of(this.FLG);

        if (Arrays.equals(NO_TIMESTAMP_AVAILABLE, 0, 4, gzipHeader, 4, 8)) {
            this.MTIME = NO_TIMESTAMP_AVAILABLE;
            this.modificationTime = null;
        } else {
            this.MTIME = Arrays.copyOfRange(gzipHeader, 4, 8);
            this.modificationTime = Instant.ofEpochSecond(ByteBuffer.wrap(this.MTIME).getInt());
        }

        this.XFL = gzipHeader[8];

        this.OS = gzipHeader[9];
        this.operatingSystem = OperatingSystem.of(this.OS);
    }

    public int getMinLength() {
        return MIN_LENGTH;
    }

    public byte getID1() {
        return ID1;
    }

    public byte getID2() {
        return ID2;
    }

    public byte[] getMagic() {
        return Gzip.MAGIC;
    }

    public byte getCM() {
        return CM;
    }

    public CompressionMethod getCompressionMethod() {
        return compressionMethod;
    }

    public byte getFLG() {
        return FLG;
    }

    public Flags getFlags() {
        return flags;
    }

    public byte[] getMTIME() {
        return MTIME;
    }

    public Instant getModificationTime() {
        return modificationTime;
    }

    public byte getXFL() {
        return XFL;
    }

    public byte getOS() {
        return OS;
    }

    public OperatingSystem getOperatingSystem() {
        return operatingSystem;
    }

    private void checkHeaderValidity(byte[] gzipHeader) {
        if (gzipHeader == null) {
            throw new NullPointerException("cannot create gzip header from null byte array");
        }

        if (gzipHeader.length != MIN_LENGTH) {
            throw new IllegalArgumentException("gzip header must be exactly " + MIN_LENGTH + " bytes");
        }

        if (gzipHeader[0] != Gzip.MAGIC[0] || gzipHeader[1] != Gzip.MAGIC[1]) {
            throw new IllegalArgumentException(
                "gzip header must start with magic number " + Integer.toHexString(ByteBuffer.wrap(Gzip.MAGIC).getInt())
            );
        }
    }

    @Override
    public String toString() {
        return "GzipHeader (flags = " + flags + ")";
    }

    public static class Flags {
        private static final Flags NO_FLAGS = Flags.of((byte) 0);

        private static final byte FTEXT = (byte) 0b10000000;
        private static final byte FHCRC = (byte) 0b01000000;
        private static final byte FEXTRA = (byte) 0b00100000;
        private static final byte FNAME = (byte) 0b00010000;
        private static final byte FCOMMENT = (byte) 0b00001000;

        private byte value;

        private Flags() { }

        public static Flags of(byte value) {
            Flags flags = new Flags();

            flags.value = value;

            return flags;
        }

        public boolean isProbableAscii() {
            return (this.value & FTEXT) != 0;
        }

        public boolean isCrc16() {
            return (this.value & FHCRC) != 0;
        }

        public boolean hasOptionalFields() {
            return (this.value & FEXTRA) != 0;
        }

        public boolean hasFileName() {
            return (this.value & FNAME) != 0;
        }

        public boolean hasComment() {
            return (this.value & FCOMMENT) != 0;
        }

        public byte getValue() {
            return value;
        }

        @Override
        public String toString() {
            return Integer.toBinaryString(value);
        }
    }
}
