package uk.co.paulbenn.gzip;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class GzipTrailer {

    public static final int LENGTH = 8;

    private final byte[] CRC32;
    private final int crc32;

    private final byte[] ISIZE;
    private final int uncompressedSize;

    public GzipTrailer(byte[] gzipTrailer) {
        checkTrailerValidity(gzipTrailer);

        this.CRC32 = reverseEndianness(Arrays.copyOfRange(gzipTrailer, 0, 4));
        this.crc32 = ByteBuffer.wrap(CRC32).getInt();

        this.ISIZE = reverseEndianness(Arrays.copyOfRange(gzipTrailer, 4, 8));
        this.uncompressedSize = ByteBuffer.wrap(ISIZE).getInt();
    }

    public int getLength() {
        return LENGTH;
    }

    public byte[] getCRC32() {
        return CRC32;
    }

    public int getCrc32() {
        return crc32;
    }

    public byte[] getISIZE() {
        return ISIZE;
    }

    public int getUncompressedSize() {
        return uncompressedSize;
    }

    @Override
    public String toString() {
        return "GzipTrailer (uncompressedSize = " + uncompressedSize + " bytes)";
    }

    private void checkTrailerValidity(byte[] gzipTrailer) {
        if (gzipTrailer == null) {
            throw new NullPointerException("cannot create gzip trailer from null byte array");
        }

        if (gzipTrailer.length != LENGTH) {
            throw new IllegalArgumentException("gzip trailer must be exactly " + LENGTH + " bytes");
        }
    }

    private byte[] reverseEndianness(byte[] source) {
        for(int i = 0; i < source.length / 2; i++) {
            byte tmp = source[i];
            source[i] = source[source.length - i - 1];
            source[source.length - i - 1] = tmp;
        }
        return source;
    }
}
