package uk.co.paulbenn.gzip;

public enum OperatingSystem {
    FAT((byte) 0),
    AMIGA((byte) 1),
    VMS((byte) 2),
    UNIX((byte) 3),
    VM_CMS((byte) 4),
    ATARI_TOS((byte) 5),
    HPFS((byte) 6),
    MACINTOSH((byte) 7),
    Z_SYSTEM((byte) 8),
    CP_M((byte) 9),
    TOPS_20((byte) 10),
    NTFS((byte) 11),
    QDOS((byte) 12),
    ACORN_RISCOS((byte) 13),
    UNKNOWN((byte) 255);

    private final byte OS;

    OperatingSystem(byte OS) {
        this.OS = OS;
    }

    public static OperatingSystem of(byte OS) {
        for (OperatingSystem operatingSystem : OperatingSystem.values()) {
            if (operatingSystem.OS == OS) {
                return operatingSystem;
            }
        }

        return UNKNOWN;
    }

    public byte getByte() {
        return OS;
    }
}
