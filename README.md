# Static GZIP utilities & inspection
A small library to deal with common GZIP operations on strings. Allows for close inspection of GZIP protocol internals such as the deflated byte array's header and trailer information.

### Dependency
Include the library (**Maven**):
```xml
<dependency>
    <groupId>uk.co.paulbenn</groupId>
    <artifactId>gzip</artifactId>
    <version>1.0</version>
</dependency>
```

Include the library (**Gradle**):
```groovy
implementation("uk.co.paulbenn:gzip:1.0")
```

### Compression & Decompression
Deflate (compress) a `String`:
```java
byte[] deflated = Gzip.deflate(string);
```

Inflate (decompress) a `byte[]`:
```java
String deflated = Gzip.inflate(bytes);
```

### Inspection
Allows you to view low-level information about a GZIP byte array. A GZIP entry consists of a header, the compressed content and a trailer. To analyze the header and trailer, start by inspecting a deflated array:
```java
GzipEntry gzipEntry = Gzip.inspect(bytes);
```
This creates a `GzipHeader` and a `GzipTrailer` object from the data in the first 10 bytes and last 8 bytes of the entry.
```java
GzipHeader header = gzipEntry.getHeader();
header.getFlags();
header.getModificationTime();
// ...

GzipTrailer trailer = gzipEntry.getTrailer();
trailer.getCrc32();
trailer.getUncompressedSize();
// ...
```

### Limitations
* This library does not support GZIP headers longer than the default 10 bytes.
## JDK-8244706

The following code prints `FAT` for all strings on all operating systems and all Java versions older than `16 b16`, demonstrating a minor non-compliance issue in the JDK detailed [here](https://bugs.java.com/bugdatabase/view_bug.do?bug_id=JDK-8244706).

```java
public class Main {

    public static void main(String[] args) {
        System.out.println(
            Gzip.inspect(Gzip.deflate("Hello World"))
                .getHeader()
                .getOperatingSystem()
        );
    }
}
```
